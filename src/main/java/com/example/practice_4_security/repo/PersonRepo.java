package com.example.practice_4_security.repo;

import com.example.practice_4_security.payload.RegPerson;
import com.example.practice_4_security.payload.Role;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonRepo {
    private static List<RegPerson> personsList = new ArrayList<>();

    static {
        personsList.add(new RegPerson(1, "Ivanov", "Ivan", "login1", "password111", Role.ADMIN));
        personsList.add(new RegPerson(2, "Grigoriy", "Grigoriev", "login2", "password222", Role.USER));
        personsList.add(new RegPerson(3, "Serggev", "Sergey", "login3", "password333", Role.USER));
        personsList.add(new RegPerson(4, "Dmitriy", "Dmitriev", "login4", "password444", Role.USER));
        personsList.add(new RegPerson(5, "Efrosiniya", "Kozlova", "login5", "password555", Role.ADMIN));
    }


    public List<RegPerson> getAll() {
        return personsList;
    }

    public Optional<RegPerson> findPassByLogin(String login) {
        return personsList.stream().filter(person -> person.getLogin().equals(login)).findAny();
    }

    public void addPerson(RegPerson person) {
        personsList.add(person);
    }

    public void updatePerson(RegPerson person) {
        personsList
                .stream()
                .filter(payload -> payload.getId() == person.getId())
                .forEach(payload -> {
                    payload.setLName(person.getLName());
                    payload.setFName(person.getFName());
                    payload.setLogin(person.getLogin());
                    payload.setPassword(person.getPassword());
                    payload.setRole(person.getRole());
                });
    }

    public void deletePerson(int id) {
        personsList
                .stream()
                .filter(e -> e.getId() == id)
                .forEach(e -> personsList.remove(e));
    }
}
