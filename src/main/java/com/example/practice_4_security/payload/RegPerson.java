package com.example.practice_4_security.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegPerson {

    @NotNull(message = "ID is required")
    @Min(value = 1, message = "ID can be only positive")
    private long id;

    @NotBlank(message = "First Name is required")
    private String fName;

    @NotBlank(message = "Last name is required")
    private String lName;

    @Size(min = 6, max = 30, message = "Min limit 6, Max limit 30")
    private String login;

    @Size(min = 6, max = 30, message = "Min limit 6, Max limit 30")
    private String password;

    private Role role;
}





