package com.example.practice_4_security.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthPerson {
    @Size(min = 6, max = 30, message = "Min limit 6, Max limit 30")
    @NotBlank(message = "Login is required")
    private String login;

    @Size(min = 6, max = 30, message = "Min limit 6, Max limit 30")
    @NotBlank(message = "Password is required")
    private String password;
}
