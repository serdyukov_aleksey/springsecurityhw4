package com.example.practice_4_security.payload;

public enum Role {
    USER, ADMIN
}
