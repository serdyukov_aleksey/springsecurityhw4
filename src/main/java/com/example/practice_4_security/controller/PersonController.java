package com.example.practice_4_security.controller;


import com.example.practice_4_security.payload.RegPerson;
import com.example.practice_4_security.repo.PersonRepo;
import com.example.practice_4_security.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonRepo personRepo;

    private final JwtTokenProvider jwtTokenProvider;

    @GetMapping(value = {"/all"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RegPerson>> getAllPersons(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (isValidHeader(token)){
            return new ResponseEntity<>(personRepo.getAll(), HttpStatus.OK);
        }
            return ResponseEntity.badRequest().build();
    }

    @PutMapping(path = "/update",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(@Valid @RequestBody RegPerson person, @RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (isValidHeader(token)) {
            personRepo.updatePerson(person);
        }
    }

    @DeleteMapping(path = "/delete/{id}")
    public void deletePerson(@PathVariable int id, @RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (isValidHeader(token)) {
            personRepo.deletePerson(id);
        }
    }

    private boolean isValidHeader(String token) {
        return !token.isEmpty() && jwtTokenProvider.validateToken(token);
    }
}
