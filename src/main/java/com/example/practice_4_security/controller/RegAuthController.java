package com.example.practice_4_security.controller;

import com.example.practice_4_security.payload.AuthPerson;
import com.example.practice_4_security.payload.RegPerson;
import com.example.practice_4_security.payload.touser.AuthResponse;
import com.example.practice_4_security.repo.PersonRepo;
import com.example.practice_4_security.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/enter")
@RequiredArgsConstructor
public class RegAuthController {

    private final PersonRepo personRepo;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping(path = "/reg",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> submitReg(@Valid @RequestBody RegPerson person) {
        if (person != null) {
            personRepo.addPerson(person);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PostMapping(path = "/auth",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AuthResponse> submitAuth(@Valid @RequestBody AuthPerson authPerson) {
        Optional<RegPerson> regPerson = personRepo.findPassByLogin(authPerson.getLogin());
        if (regPerson.isPresent()) {
            if (checkAuthPass(authPerson, regPerson.get())) {
                String token = jwtTokenProvider.accessToken(regPerson.get());
                return ResponseEntity.status(HttpStatus.OK).body(new AuthResponse(token));
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    private boolean checkAuthPass(AuthPerson person, RegPerson regPerson) {
        String regPass = regPerson.getPassword();
        return regPass.equals(person.getPassword());
    }
}
