package com.example.practice_4_security.security;

import com.example.practice_4_security.payload.RegPerson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    private static final String AUTHORITIES = "AUTHORITIES";
    private static final SignatureAlgorithm ALGORITHM_HS512 = SignatureAlgorithm.HS512;

    @Value("${secret.key}")
    private String secretKey;

    @Value("${token.expiration.time}")
    private byte expirationTime;

    public String accessToken(RegPerson regPerson) {
        if (Objects.nonNull(regPerson)) {
            Date date = Date.from(LocalDate.now().plusDays(expirationTime).atStartOfDay(ZoneId.systemDefault()).toInstant());
            return Jwts.builder()
                    .setSubject(regPerson.getLogin())
                    .claim(AUTHORITIES, regPerson.getRole().name())
                    .setExpiration(date)
                    .signWith(ALGORITHM_HS512, secretKey)
                    .compact();
        }
        return null;
    }

    public String getLoginFromJwt(String token) {
        String correctToken = getTokenFromRequest(token);
        Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(correctToken)
                .getBody();
        return claims.getSubject();
    }

    public boolean validateToken(String authToken) {
        String token = getTokenFromRequest(authToken);
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }

    private String getTokenFromRequest(String token) {
        if (token != null && token.startsWith("Bearer ")) {
            return token.substring(7);
        }
        return token;
    }
}
