package com.example.practice_4_security.security;

import com.example.practice_4_security.payload.RegPerson;
import com.example.practice_4_security.repo.PersonRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final PersonRepo personRepo;

    public CustomUserDetailsService(PersonRepo personRepo) {
        this.personRepo = personRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String login){
        Optional<RegPerson> regPerson = personRepo.findPassByLogin(login);
        return regPerson.map(UserPrincipal::fromUserEntityToCustomUserDetails).orElse(null);
    }
}
