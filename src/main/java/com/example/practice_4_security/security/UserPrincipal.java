package com.example.practice_4_security.security;

import com.example.practice_4_security.payload.RegPerson;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;


public class UserPrincipal implements UserDetails {
    private String login;
    private String password;
    private final String ROLE = "ROLE_USER";
    private Collection<? extends GrantedAuthority> grantedAuthorities;

    public static UserPrincipal fromUserEntityToCustomUserDetails(RegPerson regPerson) {
        UserPrincipal up = new UserPrincipal();
        up.login = regPerson.getLogin();
        up.password = regPerson.getPassword();
        up.grantedAuthorities = Collections.singletonList(
                new SimpleGrantedAuthority(up.ROLE));
        return up;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

